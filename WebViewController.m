//
//  ViewController.m
//  Vikatan
//
//  Created by ephronsystems on 6/4/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "WebViewController.h"
#import "FrameAccessor.h"
#import "ViewController.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import "AePubReaderAppDelegate.h"
#import "ASIHTTPRequest.h"
#import "Base64.h"
#import "EPubViewController.h"
#import "SVProgressHUD.h"

//#define kTutorialPointProductID @"com.vikatan.ebookreader.2122"

@interface WebViewController ()<GPPSignInDelegate>

@end

@implementation WebViewController
@synthesize comingView;
@synthesize Activity;
@synthesize comingFrom;
NSString *html_path;
GPPSignIn *signIn;

static NSString * const kClientId = @"373257897663-uub12d3jkkgha3ea0ucle62bij6k855k.apps.googleusercontent.com";
UIAlertView *delete_alert;
NSString *common_SSO_tag;

NSString *fb_access;
NSString *gp_access;
NSString *gp_email;
NSString *gp_username;
UIAlertView *progressAlert;
NSString *inAppProductID;
NSString *purchaseBookIDStr;
NSString *purchaseStatus;

NSString *send_book_id;
NSString *send_book_name;
NSString *epubFileDownloadURL;
BOOL autoRotate;
NSString *restoreClicked;

NSString *kTutorialPointProductID;

- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"TestNotification"])
        NSLog (@"Successfully received the test notification!");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
     self.trackedViewName = @"Store Page";
       
   
    common_SSO_tag=@"";
    fb_access=@"";
    gp_access=@"";
    gp_email=@"";
    inAppProductID=@"";
     restoreClicked=@"";
    
    _web_view.opaque = NO;
    _web_view.backgroundColor = [UIColor clearColor];
    
    if ([comingFrom isEqualToString:@"FIRST"])
    {
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        
        
        NSString *data_path=[NSString stringWithFormat:@"Demo/ipad/demo.html"];
        
        html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
        
        autoRotate=NO;
        
        
       // [self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortraitUpsideDown];
    }
    else
    {
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
     autoRotate=YES;
    
    NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
    
    html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
    }
    
      
   NSLog(@"IMAGE:%@",html_path);
    
  
    
    [_web_view loadRequest:[NSURLRequest requestWithURL:
                            [NSURL fileURLWithPath:html_path]]];
       [self.view addSubview:_web_view];
     
    
   
    
    
}

 
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
     NSString *yourHTMLSourceCodeString = [webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
        
    if ([common_SSO_tag isEqualToString:@"FB"])
    {
        
        NSUserDefaults *fb_em=[NSUserDefaults standardUserDefaults];
       // [fb_em setObject:value_type forKey:@"FB_EMAIL_ID"];
        
        NSString * param  =[fb_em objectForKey:@"FB_EMAIL_ID"];
        NSString * jsCallBack = [NSString stringWithFormat:@"get_val('%@')",param];
        [_web_view stringByEvaluatingJavaScriptFromString:jsCallBack];
        
        common_SSO_tag=@"";
    }
    
    if ([common_SSO_tag isEqualToString:@"GP"])
    {
        
        NSUserDefaults *fb_em=[NSUserDefaults standardUserDefaults];
        // [fb_em setObject:value_type forKey:@"FB_EMAIL_ID"];
        
        NSString * param  =[fb_em objectForKey:@"GP_EMAIL_ID"];
        NSString * jsCallBack = [NSString stringWithFormat:@"get_val('%@')",param];
        [_web_view stringByEvaluatingJavaScriptFromString:jsCallBack];
        
         common_SSO_tag=@"";
    }
  
    if ([purchaseStatus isEqualToString:@"SUCCESS"])
    {
        
        
        //com.vikatan.ebookreader
        //com.vikatan.ebookreaderiphone
        
        
        NSUserDefaults *fb_em=[NSUserDefaults standardUserDefaults];
        // [fb_em setObject:value_type forKey:@"FB_EMAIL_ID"];
        
        purchaseBookIDStr=[fb_em objectForKey:@"BOOK_ID_VALUE"];
        
        
        NSString *getSTR=[fb_em objectForKey:@"PAYMENT_RECEIPT"];
       
        NSData *plainTextData = [getSTR dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64String = [plainTextData base64EncodedString];

        
        
        NSLog(@"BOOK A:%@",inAppProductID);
        NSLog(@"ID A: %@",purchaseBookIDStr);
        NSLog(@"STAT A:%@",purchaseStatus);
        
        NSString *value_final=[NSString stringWithFormat:@"%@|%@|%@",purchaseBookIDStr,purchaseStatus,base64String];
        
        NSLog(@"FINAL:%@",value_final);
        
        NSString * jsCallBack = [NSString stringWithFormat:@"get_payment('%@')",value_final];
        [_web_view stringByEvaluatingJavaScriptFromString:jsCallBack];
        
       // common_SSO_tag=@"";
        
        
        restoreClicked=@"SUCCESS";
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
        
        html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
        
        
        NSLog(@"IMAGE:%@",html_path);
        
        
        
        
        [_web_view loadRequest:[NSURLRequest requestWithURL:
                                [NSURL fileURLWithPath:html_path]]];

        
        purchaseStatus=@"";
    }

    
    if ([restoreClicked isEqualToString:@"SUCCESS"])
    {
        
        
        
        
        NSUserDefaults *fb_em=[NSUserDefaults standardUserDefaults];
       
        
        NSString   *restoreIDStr=[fb_em objectForKey:@"RESTORE_ID"];
        
                   
        
        NSLog(@"BOOK ASAS:%@",restoreIDStr);
        
        
        
        
        NSLog(@"FINAL ASAS:%@",restoreIDStr);
        
        NSString * jsCallBack = [NSString stringWithFormat:@"get_restore_val('%@')",restoreIDStr];
        [_web_view stringByEvaluatingJavaScriptFromString:jsCallBack];

        
        
        
                
        
        
          restoreClicked=@"";
      
         
        
    }
    

   
    
   // NSLog(@"HTML SOURCE CODE:%@",yourHTMLSourceCodeString);
}


- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    //CAPTURE USER LINK-CLICK.
    NSURL *url = [request URL];
    NSString *url_string=[NSString stringWithFormat:@"%@",url];
    NSString *url_string1=[NSString stringWithFormat:@"%@",url];
  
    NSLog(@"HELLO:%@",url_string);
    if ([url_string1 rangeOfString:@"restore="].location == NSNotFound)
    {
    if ([url_string1 rangeOfString:@"demo.html"].location == NSNotFound)
    {
        webView.scrollView.scrollEnabled = YES;
        webView.scrollView.bounces = YES;
         autoRotate=YES;
        
    if ([url_string1 rangeOfString:@"ephurl="].location == NSNotFound)
    {
        if ([url_string1 rangeOfString:@"videourl="].location == NSNotFound)
        {
            if ([url_string1 rangeOfString:@"twurl="].location == NSNotFound)
            {
                if ([url_string1 rangeOfString:@"fburl="].location == NSNotFound)
                {
    if ([url_string1 rangeOfString:@"logout=logout"].location == NSNotFound)
    {
        
    if ([url_string1 rangeOfString:@"preview="].location == NSNotFound)
    {
    if ([url_string1 rangeOfString:@"buy="].location == NSNotFound)
    {
    
          if ([url_string1 rangeOfString:@"fb=fb"].location == NSNotFound)
          {
        if ([url_string1 rangeOfString:@"gp=gp"].location == NSNotFound)
        {
        if ([url_string rangeOfString:@"lib=lib"].location == NSNotFound)
        {
            
            if ([url_string rangeOfString:@"sync="].location == NSNotFound)
            {
                
                
                
                
            }
            else
            {
                NSArray *strings = [url_string componentsSeparatedByString:@"sync="];
                
                NSString *id_Book=strings[1];
                NSString *user_id=strings[2];
                NSString *password_str=strings[3];
                 NSString *displayname=strings[4];
                
                
                NSLog(@"DISPLAY:%@",displayname);
                
//                id_Book = [id_Book                                            stringByReplacingOccurrencesOfString:@"&" withString:@""];
//                user_id = [user_id                                            stringByReplacingOccurrencesOfString:@"&" withString:@""];
                
                id_Book= [id_Book substringToIndex:[id_Book length]-1];
                user_id= [user_id substringToIndex:[user_id length]-1];
                 //displayname= [displayname substringToIndex:[displayname length]-1];
                
                
                NSUserDefaults *use=[NSUserDefaults standardUserDefaults];
                [use setObject:displayname forKey:@"DISPLAY_NAME"];
                
                
                
                NSLog(@"ID BOOKS:%@",id_Book);
                NSLog(@"user_id:%@",user_id);
                NSLog(@"password_str:%@",password_str);
                
                ViewController *epub=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
                epub.comingFromSync=@"YES";
                epub.syncBookID=id_Book;
                epub.username=user_id;
                epub.password=password_str;
                [self presentModalViewController:epub animated:NO];
            }
            
            
            
        }
        else
        {
            NSLog(@"Move to Library");
            
            
            
            NSArray *strings = [url_string componentsSeparatedByString:@"username="];
            
            NSString *userName=strings[1];
            NSString *useremail=strings[2];
            
            
            
            userName = [userName                                            stringByReplacingOccurrencesOfString:@"&" withString:@""];
            useremail = [useremail                                            stringByReplacingOccurrencesOfString:@"&" withString:@""];
            
            NSUserDefaults *use=[NSUserDefaults standardUserDefaults];
            [use setObject:userName forKey:@"DISPLAY_NAME"];
            [use setObject:useremail forKey:@"USERNAME_EMAIL"];
            
            NSLog(@"EMAIL:%@",useremail);
                       
            
          
            ViewController *epub=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
            epub.comingFromSync=@"NO";
            //epub.syncBookID=@"111";
            [self presentModalViewController:epub animated:NO];
            
            
        }
        }
        else
        {
            NSLog(@"ENTER GOOGLE");
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            
            NSString *data_path=[NSString stringWithFormat:@"Demo/login.html"];
            
            html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
            
            [_web_view loadRequest:[NSURLRequest requestWithURL:
                                    [NSURL fileURLWithPath:html_path]]];
            
            AePubReaderAppDelegate *appDelegate = (AePubReaderAppDelegate *)
            [[UIApplication sharedApplication] delegate];
            
            signIn = [GPPSignIn sharedInstance];
            // You previously set kClientId in the "Initialize the Google+ client" step
            signIn.clientID = kClientId;
            signIn.shouldFetchGoogleUserEmail = YES;
            signIn.scopes = [NSArray arrayWithObjects:
                             @"https://www.googleapis.com/auth/userinfo.profile",
                             @"https://www.googleapis.com/auth/userinfo.email",
                             @"http://www.google.com/m8/feeds",
                             nil];
            
                    
            signIn.delegate = self;
            
            
            
            
            
            [signIn authenticate];
             
        }

      
    } else
          {
        NSLog(@"ENTER FB");
              NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
              
              NSString *data_path=[NSString stringWithFormat:@"Demo/login.html"];
              
              html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
              
              [_web_view loadRequest:[NSURLRequest requestWithURL:
                                      [NSURL fileURLWithPath:html_path]]];
        [self faceBookClick:nil];
    }
    
    }
    else
    {
        NSLog(@"ENTER PAYMENT");
        
        NSArray *strings = [url_string componentsSeparatedByString:@"buy="];
        
        NSString *id_Book=strings[1];
        NSString *ios_uid=strings[2];
      
        
        
        id_Book = [id_Book                                            stringByReplacingOccurrencesOfString:@"&" withString:@""];
        ios_uid = [ios_uid                                            stringByReplacingOccurrencesOfString:@"&" withString:@""];
        
        
        
        NSLog(@"ID BOOKS:%@",id_Book);
        NSLog(@"user_id:%@",ios_uid);
      
        
        
      //  inAppProductID=@"com.vikatan.ebookreader.2122";
        
        inAppProductID=ios_uid;
        kTutorialPointProductID=ios_uid;
        
        NSUserDefaults *sta=[NSUserDefaults standardUserDefaults];
        [sta setObject:id_Book forKey:@"BOOK_ID_VALUE"];
        
        purchaseBookIDStr=id_Book;
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        [check setObject:@"NO" forKey:@"RESTORE"];
        
        [self purchase:inAppProductID];
    }
    }
    else
    {
        NSLog(@"ENTER PREVIEW");
        
        NSArray *strings = [url_string componentsSeparatedByString:@"preview="];
        
        NSString *id_Book1=strings[1];
        NSString *ios_url=strings[2];
        
         NSString *ios_name=strings[3];
        
//        id_Book1 = [id_Book1                                            stringByReplacingOccurrencesOfString:@"&" withString:@""];
//        ios_url = [ios_url                                            stringByReplacingOccurrencesOfString:@"&" withString:@""];
//        
//         ios_name = [ios_name                                            stringByReplacingOccurrencesOfString:@"&" withString:@""];
        
        ios_url= [ios_url substringToIndex:[ios_url length]-1];
        ios_name= [ios_name substringToIndex:[ios_name length]-1];
        id_Book1= [id_Book1 substringToIndex:[id_Book1 length]-1];
        
        NSLog(@"ID BOOKS:%@",id_Book1);
        NSLog(@"user_id:%@",ios_url);
          NSLog(@"user_id:%@",ios_name);
        
        
        
        
            
            
            send_book_id=[NSString stringWithFormat:@"Prev_%@",id_Book1];
           send_book_name=[NSString stringWithFormat:@"%@",@""];
           epubFileDownloadURL=[NSString stringWithFormat:@"%@",ios_url];
            
            
            NSLog(@"BOOK ID:%@",send_book_id);
            NSLog(@"BOOK NAME:%@",send_book_name);
            NSLog(@"BOOK URL:%@",epubFileDownloadURL);
            
            
            NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
            [passValues setObject:send_book_id forKey:@"BOOK_ID"];
            [passValues setObject:send_book_id forKey:@"BOOK_EPUB"];
            [passValues setObject:send_book_name forKey:@"BOOK_NAME"];
        
        
        progressAlert = [[UIAlertView alloc] initWithTitle: @"Loading..." message: @"Please wait..." delegate: self cancelButtonTitle: nil otherButtonTitles: nil];
        
        Activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(90.0f, 40.0f, 100.0f, 100.0f)];
        
        
        
        [progressAlert addSubview:Activity];
        
        [Activity startAnimating];
        
        [progressAlert show];
        [progressAlert release];
        
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
        NSString* foofile = [documentsPath stringByAppendingPathComponent:epub_name];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
        
        
        if (fileExists)
        {
            NSLog(@"EXISTS");
            
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.height == 480)
                {
                    // iPhone Classic
                    
                    NSLog(@"OLD IPHONE");
                }
                if(result.height == 568)
                {
                    // iPhone 5
                    EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone5" bundle:nil];
                    
                    //epub.epubName=@"7";
                    
                    epub.epubName=send_book_id;
                    
                    epub.comingView=@"PREVIEW";
                    
                    [self presentModalViewController:epub animated:NO];            }
            }
            else
            {
                NSLog(@"iPAd");
                
                
                
                
                EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EPubView" bundle:nil];
                epub.epubName=send_book_id;
                // epub.epubName=@"7777";
                epub.comingView=@"PREVIEW";
                [self presentModalViewController:epub animated:NO];
            }
            
            [Activity stopAnimating];
            [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
            
            
        }
        else
        {
            
            NSLog(@"NOT EXISTS");
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            
            
            // threadProgressView.hidden=NO;
            
            
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSURL *url=[NSURL URLWithString:epubFileDownloadURL];
            
            
            NSLog(@"Dowload Start");
            
            ASIHTTPRequest  *request = [ASIHTTPRequest requestWithURL:url];
            //  [collectionView reloadData];
            
            [request setShouldContinueWhenAppEntersBackground:YES];
             [request setTimeOutSeconds:240];
            
            [request setCompletionBlock:^{
                // Use when fetching text data
                
                NSString *responseString = [request responseString];
                
                
                
                // Use when fetching binary data
                NSData *responseData = [request responseData];
                
                NSString *epub_name=[NSString stringWithFormat:@"%@.epub",send_book_id];
                
                
                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:epub_name];
                
                [responseData writeToFile:filePath atomically:YES];
                
                NSLog(@"Dowload Completes");
                
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                // [self Load:nil];
                
                
                NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
                [passValues setObject:send_book_id forKey:@"BOOK_ID"];
                [passValues setObject:send_book_id forKey:@"BOOK_EPUB"];
                [passValues setObject:send_book_name forKey:@"BOOK_NAME"];
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    CGSize result = [[UIScreen mainScreen] bounds].size;
                    if(result.height == 480)
                    {
                        // iPhone Classic
                        
                        NSLog(@"OLD IPHONE");
                    }
                    if(result.height == 568)
                    {
                        // iPhone 5
                        EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EpubView~iPhone5" bundle:nil];
                        
                        //epub.epubName=@"7";
                        
                        epub.epubName=send_book_id;
                        epub.comingView=@"PREVIEW";
                        [self presentModalViewController:epub animated:NO];            }
                }
                else
                {
                    NSLog(@"iPAd");
                    
                    EPubViewController *epub=[[EPubViewController alloc]initWithNibName:@"EPubView" bundle:nil];
                    epub.epubName=send_book_id;
                    // epub.epubName=@"7777";
                    epub.comingView=@"PREVIEW";
                    [self presentModalViewController:epub animated:NO];
                }
                [Activity stopAnimating];
                [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
                
            }];
            [request setFailedBlock:^
             {
                 // NSError *error = [request error];
                 
                 
                 UIAlertView *error_Alert=[[UIAlertView alloc]initWithTitle:@"Download Error" message:[NSString stringWithFormat:@"%@",[request error]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 
                 [error_Alert show];
                 [Activity stopAnimating];
                 [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
                 
             }];
            [request startAsynchronous];
            
            
            
        }

        
        
        
      //  [self performSelector:@selector(goto_next2) withObject:nil afterDelay:3];
        
                    
            
            
        
        
        
        
        
        
        //  inAppProductID=@"com.vikatan.ebookreader.2122";
        
        
        
    }
    }
    else
    {
        NSLog(@"LOGOUT");
        [self signOut];
        
        
        NSUserDefaults *use=[NSUserDefaults standardUserDefaults];
        [use setObject:@"" forKey:@"DISPLAY_NAME"];
         [use setObject:@"" forKey:@"USERNAME_EMAIL"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults removeObjectForKey:@"FBAccessTokenKey"];
        [defaults removeObjectForKey:@"FBExpirationDateKey"];
        
        [defaults synchronize];
        
        NSHTTPCookie *cookie;
        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (cookie in [storage cookies]) {
            [storage deleteCookie:cookie];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
              
                }
                else
                {
                    NSLog(@"FB URL");
                    
                    
                    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    
                    NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
                    
                    html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
                    [_web_view loadRequest:[NSURLRequest requestWithURL:
                                            [NSURL fileURLWithPath:html_path]]];
                    
                    NSURL* facebookURL = [ NSURL URLWithString: @"https://facebook.com/VikatanPublications" ];
                    NSURL* facebookAppURL = [ NSURL URLWithString: @"fb://profile/119336181556131" ];
                    
                    
                    
                    UIApplication* app = [ UIApplication sharedApplication ];
                    if( [ app canOpenURL: facebookAppURL ] ) {
                        [ app openURL: facebookAppURL ];
                    } else {
                        [ app openURL: facebookURL ];
                    }

                    
                   // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/VikatanPublications"]];
                }
            }
            else
            {
                NSLog(@"TW URL");
                
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                
                NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
                
                html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
                [_web_view loadRequest:[NSURLRequest requestWithURL:
                                        [NSURL fileURLWithPath:html_path]]];
                
                
                NSURL* twitterURL = [ NSURL URLWithString: @"https://twitter.com/vikatan/" ];
                NSURL* twitterAppURL = [ NSURL URLWithString: @"twitter://user?screen_name=vikatan" ];
                
                
                UIApplication* app = [ UIApplication sharedApplication ];
                if( [ app canOpenURL: twitterAppURL ] ) {
                    [ app openURL: twitterAppURL ];
                } else {
                    [ app openURL: twitterURL ];
                }

                
                
                
               // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/vikatan/"]];
            }
        }
        else
        {
            NSLog(@"VI URL");
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://videos.vikatan.com/"]];
        }
    }
    else
    {
        NSLog(@"EPH URL");
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
        
        html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
        [_web_view loadRequest:[NSURLRequest requestWithURL:
                                [NSURL fileURLWithPath:html_path]]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.ephronsystems.com"]];
    }
    }
    else
    {
        
        webView.scrollView.scrollEnabled = NO;
        webView.scrollView.bounces = NO;
         autoRotate=NO;
        
    }
    }
    else
    {
        NSLog(@"Restore");
        
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
        [[SKPaymentQueue defaultQueue]restoreCompletedTransactions];
    }
    
    return YES;
}

- (void)signOut {
    [[GPPSignIn sharedInstance] signOut];
}


-(IBAction)purchase:(id)sender
{
    
       //[activityIndicatorView startAnimating];
    //Hide purchase button initially
    //purchaseButton.hidden = YES;
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    
    [self fetchAvailableProducts:sender];
    
}



-(void)fetchAvailableProducts:(NSString *)product_id

{
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects:product_id,nil];
    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}

- (void)purchaseMyProduct:(SKProduct*)product{
    if ([self canMakePurchases])
    {
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        
        NSString *val=[check objectForKey:@"RESTORE"];
        
        if ([val isEqualToString:@"YES"])
        {
            
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            
            [[SKPaymentQueue defaultQueue]restoreCompletedTransactions];
            
        }
        else
        {
            
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            
            SKPayment *payment = [SKPayment paymentWithProduct:product];
            
            
            [[SKPaymentQueue defaultQueue] addPayment:payment];
        }

    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"Purchases are disabled in your device" message:nil delegate:
                                  self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
        
        //[activityIndicatorView stopAnimating];
        
        [SVProgressHUD dismiss];
    }
}


- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSLog(@"%@",queue );
    NSLog(@"Restored Transactions are once again in Queue for purchasing %@",[queue transactions]);
    
    NSMutableArray *purchasedItemIDs = [[NSMutableArray alloc] init];
    NSLog(@"received restored transactions: %i", queue.transactions.count);
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    
    
    for (SKPaymentTransaction *transaction in queue.transactions)
    {
        NSString *productID = transaction.payment.productIdentifier;
        [purchasedItemIDs addObject:productID];
        NSLog (@"product id is %@" , productID);
        
        //[arr addObject:productID];
        
        
        
        Dataware *dd=[[Dataware alloc]initDataware];
        
        NSString *sstr=@"";
        
        sstr=@"INSERT INTO tb_inApp(inAppid) VALUES(?)";
        
        
        sqlite3_stmt *stmt=[dd OpenSQL:[sstr UTF8String]];
        if (stmt!=nil)
        {
            sqlite3_bind_text(stmt,1,[productID UTF8String],-1,SQLITE_TRANSIENT);
            
            
            sqlite3_step(stmt);
        }
        sqlite3_finalize(stmt);
        
        
        
        
        
        
        
        
        // here put an if/then statement to write files based on previously purchased items
        // example if ([productID isEqualToString: @"youruniqueproductidentifier]){write files} else { nslog sorry}
    }
    
    
    
    NSString *querystring=[NSString stringWithFormat:@"select * from tb_inApp"];
    
    const char *sqlvalue = [querystring UTF8String];
    Dataware *dbsql=[[Dataware alloc]initDataware];
    sqlite3_stmt *sqlStmt=[dbsql OpenSQL:sqlvalue];
    
    
    
    NSMutableArray *inAppIdArr=[[NSMutableArray alloc]init];
    
    if(sqlStmt != nil)
        while(sqlite3_step(sqlStmt) == SQLITE_ROW)
        {
            
            //NSLog(@"UU");
            
            NSString *id_str_val;
            @try {
                id_str_val=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 0)];
                
            }
            @catch (NSException *exception) {
                id_str_val=@"";
            }
            
            [inAppIdArr addObject:id_str_val];
            
            
            
        }
    
    NSLog(@"BOOK ID:%@",inAppIdArr);
    
    
    
    
    NSMutableString* message = [NSMutableString stringWithCapacity:100];
    
    if ([inAppIdArr count]==1) {
        
        message=inAppIdArr[0];
    }
    else
    {
        for (int i=0; i<[inAppIdArr count]; i++)
        {
            
            if (i==0) {
                
            }
            else
            {
                [message appendString:[NSString stringWithFormat:@"%@,",inAppIdArr[i]]];
            }
        }
        
        
        
        [message deleteCharactersInRange:NSMakeRange([message length] - 1, 1)];
    }
    NSLog(@"MESSAGEGHGH:%@",message);
    
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    [check setObject:message forKey:@"RESTORE_ID"];
    
    
    restoreClicked=@"SUCCESS";
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
    
    html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
    
    
    NSLog(@"IMAGE:%@",html_path);
    
    
    
    
    [_web_view loadRequest:[NSURLRequest requestWithURL:
                            [NSURL fileURLWithPath:html_path]]];
    
    
}

#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                 //[activityIndicatorView startAnimating];
                
                [SVProgressHUD showWithStatus:@"Purchasing..."];

                
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:inAppProductID])
                {
                    NSLog(@"Purchased ");
                    
                    purchaseStatus=@"SUCCESS";
                    
                    NSString *receiptData = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
                    
                    
                    NSUserDefaults *addval=[NSUserDefaults standardUserDefaults];
                    
                    [addval setObject:receiptData forKey:@"PAYMENT_RECEIPT"];
                    
                    NSLog(@"RECEIPT:%@",receiptData);
                    NSLog(@"DATE:%@",transaction.transactionDate);
                     NSLog(@"IDEN:%@",transaction.transactionIdentifier);
                    NSLog(@"INAPP:%@",transaction.payment.productIdentifier);
                    
                    
                    
                    Dataware *dd=[[Dataware alloc]initDataware];
                    
                    NSString *sstr=@"";
                    
                    sstr=@"INSERT INTO tb_inApp(inAppid) VALUES(?)";
                    
                    
                    sqlite3_stmt *stmt=[dd OpenSQL:[sstr UTF8String]];
                    if (stmt!=nil)
                    {
                        sqlite3_bind_text(stmt,1,[transaction.payment.productIdentifier UTF8String],-1,SQLITE_TRANSIENT);
                        
                        
                        sqlite3_step(stmt);
                    }
                    sqlite3_finalize(stmt);
                    
                    
                    NSString *querystring=[NSString stringWithFormat:@"select * from tb_inApp"];
                    
                    const char *sqlvalue = [querystring UTF8String];
                    Dataware *dbsql=[[Dataware alloc]initDataware];
                    sqlite3_stmt *sqlStmt=[dbsql OpenSQL:sqlvalue];
                    
                    
                    
                    NSMutableArray *inAppIdArr=[[NSMutableArray alloc]init];
                    
                    if(sqlStmt != nil)
                        while(sqlite3_step(sqlStmt) == SQLITE_ROW)
                        {
                            
                            //NSLog(@"UU");
                            
                            NSString *id_str_val;
                            @try {
                                id_str_val=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 0)];
                                
                            }
                            @catch (NSException *exception) {
                                id_str_val=@"";
                            }
                            
                            [inAppIdArr addObject:id_str_val];
                            
                            
                            
                        }
                    
                    NSLog(@"BOOK ID:%@",inAppIdArr);
                    
                    
                    
                    
                    NSMutableString* message = [NSMutableString stringWithCapacity:100];
                    
                    if ([inAppIdArr count]==1) {
                        
                        message=inAppIdArr[0];
                    }
                    else
                    {
                    for (int i=0; i<[inAppIdArr count]; i++)
                    {
                        
                        if (i==0) {
                            
                        }
                        else
                        {
                            [message appendString:[NSString stringWithFormat:@"%@,",inAppIdArr[i]]];
                        }
                    }
                    
                    
                                      
                    [message deleteCharactersInRange:NSMakeRange([message length] - 1, 1)];
                    }
                    NSLog(@"MESSAGEGHGH:%@",message);
                    
                    
                    
                    
                    
                    
                    
                    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
                    [check setObject:message forKey:@"RESTORE_ID"];
                    
                    
                    
                    
                    restoreClicked=@"SUCCESS";
                    
                    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
                    
                    html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
                    
                    
                    NSLog(@"IMAGE:%@",html_path);
                    
                    
                    
                    
                    [_web_view loadRequest:[NSURLRequest requestWithURL:
                                            [NSURL fileURLWithPath:html_path]]];
                    
                    
                    
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alertView show];
                    
                    //[activityIndicatorView stopAnimating];
                    
                    
                    [SVProgressHUD dismiss];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                
                
                 purchaseStatus=@"SUCCESS";
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
                
                html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
                
                
                NSLog(@"IMAGE:%@",html_path);
                
                
                
                [_web_view loadRequest:[NSURLRequest requestWithURL:
                                        [NSURL fileURLWithPath:html_path]]];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                //[activityIndicatorView stopAnimating];
                [SVProgressHUD dismiss];
                 
               
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                 purchaseStatus=@"failed";
                //[activityIndicatorView stopAnimating];
                 [SVProgressHUD dismiss];
                break;
            default:
                break;
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    int count = [response.products count];
    if (count>0)
    {
        validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];
        
        NSLog(@"VALID:%@",validProduct);
        
        if ([validProduct.productIdentifier
             isEqualToString:kTutorialPointProductID])
        {
            
            NSLog( @"Product Title: %@",validProduct.localizedTitle);
            NSLog(@"Product Desc: %@",validProduct.localizedDescription);
            NSLog(@"Product Price: %@",validProduct.price);
            
            [self purchaseMyProduct:validProducts[0]];
        }
    } else {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Not Available"
                            message:@"No products to purchase"
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];
        
        //[activityIndicatorView stopAnimating];
         [SVProgressHUD dismiss];
    }
    
    
}





- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *)error
{
    if(!error)
    {
        // Get the email address.
        NSLog(@"EMAIL : %@", signIn.authentication.userEmail);
        
        
        
        NSLog(@"EMAIL:%@",[NSString stringWithFormat:@"%@",
                           [GPPSignIn sharedInstance].authentication.userEmail]);
        
        
        NSLog(@"USER_ID:%@",[NSString stringWithFormat:@"%@",
                           [GPPSignIn sharedInstance].authentication.userID]);
        
        NSLog(@"TOKEN KEY:%@",[NSString stringWithFormat:@"%@",
                               [GPPSignIn sharedInstance].authentication.accessToken]);
        
        NSLog(@"TOKEN KEY:%@",[NSString stringWithFormat:@"%@",
                               [GPPSignIn sharedInstance].authentication.parameters]);
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?access_token=%@",[GPPSignIn sharedInstance].authentication.accessToken]];
        __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setCompletionBlock:^{
            // Use when fetching text data
            NSString *responseString = [request responseString];
            
            
            NSMutableData *results1 = [responseString JSONValue];
            
            NSLog(@"HELLO:%@",results1);
            
            
            common_SSO_tag=@"GP";
            gp_access= [GPPSignIn sharedInstance].authentication.accessToken;
            gp_email=[GPPSignIn sharedInstance].authentication.userEmail;
            gp_username=[results1 valueForKey:@"name"];
            fb_access=@"";
            
            
            NSUserDefaults *fb_em=[NSUserDefaults standardUserDefaults];
            [fb_em setObject:[NSString stringWithFormat:@"%@|%@|%@",gp_email,gp_username,gp_access] forKey:@"GP_EMAIL_ID"];
            
                        
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
            
            html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
            
            
            NSLog(@"IMAGE:%@",html_path);
            
            
            
            [_web_view loadRequest:[NSURLRequest requestWithURL:
                                    [NSURL fileURLWithPath:html_path]]];
            
            

          
            
        }];
        [request setFailedBlock:^{
            NSError *error = [request error];
        }];
        [request startAsynchronous];
        
        
              
    }
    else
    {
        NSLog(@"ERROR");
    }
}


-(IBAction)faceBookClick:(id)sender
{
    
    [self sen2];
}
-(void)sen2
{
    AePubReaderAppDelegate *getdelegate=(AePubReaderAppDelegate *)[[UIApplication sharedApplication]delegate];
    getdelegate.facebook.sessionDelegate=self;
    
    {
        
        
        [getdelegate.facebook authorize:[NSArray arrayWithObjects:                                        @"email",
                                         @"user_hometown",
                                         @"user_location",
                                         @"user_about_me",
                                                                nil]];
        
    }
}
- (void)fbDidLogin
{
    AePubReaderAppDelegate *getdelegate=(AePubReaderAppDelegate *)[[UIApplication sharedApplication]delegate];
    //[[NSUserDefaults standardUserDefaults] setValue: getdelegate.facebook.accessToken forKey: @"access_token"];
    //[[NSUserDefaults standardUserDefaults] setValue: getdelegate.facebook.expirationDate forKey: @"expiration_date"];
    [getdelegate.facebook requestWithGraphPath:@"me" andDelegate:self];
    
    NSLog(@"Logined into facebook");
    
}

- (void) request:(FBRequest*)request didLoad:(id)result
{
    AePubReaderAppDelegate *getdelegate=(AePubReaderAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSLog(@"TOKEN:%@",getdelegate.facebook.accessToken);
    
    
    NSString *facebookAccessToken=[NSString stringWithFormat:@"%@",getdelegate.facebook.accessToken];
    
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if ([result isKindOfClass:[NSDictionary class]])
    {
        
        
        NSString  *email_str123=[result objectForKey: @"email"];
        NSString *userID=[result objectForKey: @"name"];
        
        NSLog(@"EMAIL ID:%@",userID);
        
        
        
        NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
        
        NSString *val=[checkval objectForKey:@"ACCESS_TOKEN"];
        
        common_SSO_tag=@"FB";
        gp_access= @"";
        gp_email=@"";
        
        
        NSString *value_type=@"";
        NSString *username=@"";
        
        @try {
            value_type=email_str123;
            username=userID;
        }
        @catch (NSException *exception) {
            value_type=userID;
        }
        
        
        
        
        if ([value_type length]==0)
        {
            value_type=@"";
        }
        
        fb_access=value_type;
        
        NSLog(@"VALUE:%@",value_type);
        
        NSLog(@"CHECK CHECK:%@",[NSString stringWithFormat:@"%@|%@|%@",value_type,username,facebookAccessToken]);
        
        NSString *fbfbstr=[NSString stringWithFormat:@"%@|%@|%@",value_type,username,facebookAccessToken];
        
        
        
        NSUserDefaults *fb_em=[NSUserDefaults standardUserDefaults];
        
        
        [fb_em setObject:fbfbstr forKey:@"FB_EMAIL_ID"];
        
        NSLog(@"Reached");
        
        if ([value_type length]==0)
        {
            
            common_SSO_tag=@"";
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *data_path=[NSString stringWithFormat:@"Demo/login.html"];
            
            html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
            
            
            NSLog(@"IMAGE:%@",html_path);
            
            
            
            [_web_view loadRequest:[NSURLRequest requestWithURL:
                                    [NSURL fileURLWithPath:html_path]]];
        }
        else
        {
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
            
            html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
            
            
            NSLog(@"IMAGE:%@",html_path);
            
            
            
            [_web_view loadRequest:[NSURLRequest requestWithURL:
                                    [NSURL fileURLWithPath:html_path]]];
        }
        
        
        
        
        
        
        
    }
    //[getdelegate  fbauthenticationDidFinish:self];
    //[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"FacebookEnginePosted" object:result]];
}
-(void)fbDidNotLogin:(BOOL)cancelled
{
    NSLog(@"Login Cancelled");
    // [DownloadManager showAlert:@"Facebook login cancelled" :@"Warning!"];
}
-(void)fbDidExtendToken:(NSString *)accessToken expiresAt:(NSDate *)expiresAt
{
}
- (void)fbDidLogout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"FBAccessTokenKey"];
    [defaults removeObjectForKey:@"FBExpirationDateKey"];
    
    [defaults synchronize];
    
    //        Finding the Facebook Cookies and deleting them
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:
                                [NSURL URLWithString:@"http://login.facebook.com"]];
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    
}


- (void)logout:(id<FBSessionDelegate>)delegate {
    // [self logout];
    
    
    AePubReaderAppDelegate *getdelegate=(AePubReaderAppDelegate *)[[UIApplication sharedApplication]delegate];
    getdelegate.facebook.sessionDelegate=self;
    
    if (delegate != getdelegate.facebook.sessionDelegate &&
        [delegate respondsToSelector:@selector(fbDidLogout)]) {
        [delegate fbDidLogout];
    }
}
-(void)sen1
{
    
    
    
    
     NSString *client_id = @"371550522972620";
    
    
   // NSString *client_id = FB_KEY;
    
    self->fbGraph = [[FbGraph alloc] initWithFbClientID:client_id];
    
    [fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback) andExtendedPermissions:@"email"];
    
    
    
}
-(void)fbGraphCallback
{
    
    
    
    FbGraphResponse *fb_graph_response = [fbGraph doGraphGet:@"me" withGetVars:nil];
    
       // AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //if (appDelegate.session.isOpen) {
    SBJSON *parser = [[SBJSON alloc] init];
    
    //NSData *response = fb_graph_response.htmlResponse;
    
    
    
    NSString *json_string = fb_graph_response.htmlResponse;
    
    
    
    NSDictionary *statuses = [parser objectWithString:json_string error:nil];
    
    NSLog(@"STATUS:%@",statuses);
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *val=[checkval objectForKey:@"ACCESS_TOKEN"];
    
    common_SSO_tag=@"FB";
    gp_access= @"";
    gp_email=@"";
    
    
    NSString *value_type=@"";
     NSString *username=@"";
    
    @try
    {
        value_type=[statuses valueForKey:@"email"];
         username=[statuses valueForKey:@"name"];
    }
    @catch (NSException *exception)
    {
        value_type=[statuses valueForKey:@"name"];
    }
   
 
    
    
    if ([value_type length]==0)
    {
         value_type=@"";
    }
    
    fb_access=value_type;
    
       NSLog(@"VALUE:%@",value_type);
    
    NSUserDefaults *fb_em=[NSUserDefaults standardUserDefaults];
    [fb_em setObject:[NSString stringWithFormat:@"%@|%@|%@",value_type,username,val] forKey:@"FB_EMAIL_ID"];
    
    
    NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setObject:@"FB_ACCESS" forKey:NSHTTPCookieName];
    [cookieProperties setObject:val forKey:NSHTTPCookieValue];
    [cookieProperties setObject:@"www.vikatan.com" forKey:NSHTTPCookieDomain];
    [cookieProperties setObject:@"www.vikatan.com" forKey:NSHTTPCookieOriginURL];
    [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
    [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
    
    // set expiration to one month from now or any NSDate of your choosing
    // this makes the cookie sessionless and it will persist across web sessions and app launches
    /// if you want the cookie to be destroyed when your app exits, don't set this
    [cookieProperties setObject:[[NSDate date] dateByAddingTimeInterval:2629743] forKey:NSHTTPCookieExpires];
    
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    
    
    
    if ([value_type length]==0)
    {
        
        common_SSO_tag=@"";
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *data_path=[NSString stringWithFormat:@"Demo/login.html"];
        
        html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
        
        
        NSLog(@"IMAGE:%@",html_path);
        
        
        
        [_web_view loadRequest:[NSURLRequest requestWithURL:
                                [NSURL fileURLWithPath:html_path]]];
    }
    else
    {
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *data_path=[NSString stringWithFormat:@"Demo/index.html"];
        
        html_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,data_path];
        
        
        NSLog(@"IMAGE:%@",html_path);
        
        
        
        [_web_view loadRequest:[NSURLRequest requestWithURL:
                                [NSURL fileURLWithPath:html_path]]];
    }
       
  
    
  
 
    
}

////// FACEBOOK END //
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    
    NSLog(@"shouldAutorotate");
    
    return YES;
}


-(NSInteger)supportedInterfaceOrientations{
    if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]))
    {
        NSLog(@"PORTRAIT");
       
        self.web_view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Default-Portrait~ipad.png"]];
        // NSLog(@"PORT");
        // Adding activity indicator
        activityIndicatorView = [[UIActivityIndicatorView alloc]
                                 initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //activityIndicatorView.center = self.view.center;
        activityIndicatorView.frame=CGRectMake(450, 350, 30, 30);
        
        [activityIndicatorView hidesWhenStopped];
        [_web_view addSubview:activityIndicatorView];

        activityIndicatorView.frame=CGRectMake(450, 350, 30, 30);
        
    }
    
    else
    {
        NSLog(@"LANDSCAPE");
       
               self.web_view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Default-Landscape~ipad.png"]];
        // Adding activity indicator
        activityIndicatorView = [[UIActivityIndicatorView alloc]
                                 initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //activityIndicatorView.center = self.view.center;
        activityIndicatorView.frame=CGRectMake(450, 350, 30, 30);
        
        [activityIndicatorView hidesWhenStopped];
        [_web_view addSubview:activityIndicatorView];

       activityIndicatorView.frame=CGRectMake(570, 250, 30, 30);
       
        
    }
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)orientation
                                duration:(NSTimeInterval)duration
{
    if (UIDeviceOrientationIsPortrait(orientation))
    {
        
        NSLog(@"PORT");
        // Adding activity indicator
        activityIndicatorView = [[UIActivityIndicatorView alloc]
                                 initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //activityIndicatorView.center = self.view.center;
        activityIndicatorView.frame=CGRectMake(450, 350, 30, 30);
        
        [activityIndicatorView hidesWhenStopped];
        [_web_view addSubview:activityIndicatorView];

        activityIndicatorView.frame=CGRectMake(450, 350, 30, 30);
    }
    
    else
    {
        // Adding activity indicator
        activityIndicatorView = [[UIActivityIndicatorView alloc]
                                 initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //activityIndicatorView.center = self.view.center;
        activityIndicatorView.frame=CGRectMake(450, 350, 30, 30);
        
        [activityIndicatorView hidesWhenStopped];
        [_web_view addSubview:activityIndicatorView];

        activityIndicatorView.frame=CGRectMake(570, 250, 30, 30);
        
        NSLog(@"LAND");
        
    }
}



@end
