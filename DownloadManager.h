//
//  DownloadManager.h
//  Findme
//
//  Created by Apple on 22/05/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//


//enUpX7iMbpb1FSnqz8ECqB632I5iD2U3Ze


#import <Foundation/Foundation.h>

#define mainUrl @"http://www.supermain.com.my/"


//#define mainUrl @"http://192.168.1.243:1057/"
#define userId @"userid"
#define infor @"Information"

@interface DownloadManager : NSObject
{
    id callingclassObject;
    NSMutableData *responseData;
    NSMutableDictionary *dataDictionary;
    NSString* keyString;
}

@property(strong,nonatomic) NSMutableDictionary *dataDictionary;
@property(strong,nonatomic) NSMutableData *responseData;
@property(strong,nonatomic) NSString* keyString;

-(void)startDownloadingFromURL:(NSString *)url delegate:(id)delegate :(NSString*)key;
-(void)didFinishLoading:(NSDictionary*)responcedic key:(NSString*)key;
+(void)showAlert:(NSString*)msg :(NSString*)title;
@end
