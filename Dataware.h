//
//  Dataware.h
//  CoreData
//
//  Created by iMedDoc on 8-2-10.
//  Copyright 2010 Adya Solutiona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface Dataware : NSObject 
{
	NSString *databaseName;
	NSString *databasePath;
	sqlite3 *dbsql;
}

@property (nonatomic, retain) NSString *databaseName;
@property (nonatomic, retain) NSString *databasePath;

-(id)initDataware;
-(sqlite3_stmt *)OpenSQL:(const char *)stmt;
- (void)CloseSQL;

@end
