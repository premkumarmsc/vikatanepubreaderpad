
var head = document.getElementsByTagName('head')[0];
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = 'cordova-2.5.0';

var createStatement = "CREATE TABLE IF NOT EXISTS library (id INTEGER PRIMARY KEY AUTOINCREMENT, bookid INTEGER, bookname TEXT, bookimage TEXT, stored_path TEXT, status Text)";
 
var selectAllStatement = "SELECT * FROM library";

var selectidStatement = "SELECT * FROM library WHERE bookid=?";

var searchStatement = "SELECT * FROM library WHERE bookname = ? ";
 
var insertStatement = "INSERT INTO library (bookid, bookname, bookimage, stored_path, status) VALUES (?, ?, ?, ?, ?)";
 
var updateStatement = "UPDATE library SET bookid = ?, bookname = ? WHERE id=?";
 
var deleteStatement = "DELETE FROM library WHERE id=?";
 
var dropStatement = "DROP TABLE library";
 
 
 
var dataset;
 
var DataType;
 
 function initDatabase()  // Function Call When Page is ready.
 {
	 
var db = openDatabase("VikatanLib", "1.0", "Vikatan eBook", 200000);  // Open SQLite Database
    try {
 
        if (!window.openDatabase)  // Check browser is supported SQLite or not.
 
        {
 
            alert('Databases are not supported in this browser.');
 
        }
 
        else {
 
            createTable();  // If supported then call Function for create table in SQLite
 
        }
 
    }
 
    catch (e) {
 
        if (e == 2) {
 
            // Version number mismatch. 
 
            console.log("Invalid database version.");
 
        } else {
 
            console.log("Unknown error " + e + ".");
 
        }
 
        return;
 
    }
 
}
 
function createTable()  // Function for Create Table in SQLite.
 
{ 
 
    db.transaction(function (tx) { tx.executeSql(createStatement, [], showRecords, onError); });
 
}
 
function insertRecord(bookid) // Get value from Input and insert record . Function Call when Save/Submit Button Click..
 
{   
    alert('insert');
	var bookidtemp = bookid;
 
 
 	po_type = 'book_detail'
	$.ajax({
		type: "POST",
		cache: false,
		url: url,
		data: { po_type : po_type, bookid: bookid },
		error: function(xhr, settings, exception){
			console.log('The update server could not be contacted.');
		},
		beforeSend: function(){					
		$("#loading_disp").show();
		},
		success: function(data1){
			alert(data1);
			// success code
			$("#loading_disp").hide();
			var obj = $.parseJSON(data1);
			//alert(obj);
			//obj.category[0].category_list.length
//			alert(obj.book[0].book_list[0].b_name);
			var book_count = obj.book[0].book_list.length;

			
			var book_cnt = "";
			
			pur_name = obj.book[0].book_list[0].b_name;
			pur_img = obj.book[0].book_list[0].wrapper_src;
			//alert(pur_name);
			//alert(obj.book[0].book_list[0].wrapper_src);			
			
			//alert(pur_img);
  
        var booknametemp = pur_name;
		var statustemp = 'downloading';
		var bookimagetemp = pur_img;
		var storedpathtemp = '';
		
        db.transaction(function (tx) { tx.executeSql(insertStatement, [bookidtemp, booknametemp, bookimagetemp, storedpathtemp, statustemp], loadAndReset, onError); });
 
        //tx.executeSql(SQL Query Statement,[ Parameters ] , Sucess Result Handler Function, Error Result Handler Function );
			
			
		}
	});	
  
  			
 
}
 
function deleteRecord(id) // Get id of record . Function Call when Delete Button Click..
 
{
 
    var iddelete = id.toString();
 
    db.transaction(function (tx) { tx.executeSql(deleteStatement, [id], showRecords, onError); alert("Deleted Sucessfully"); });
 
    //resetForm();
 
}
 
function updateRecord() // Get id of record . Function Call when Delete Button Click..
 
{
 
    var usernameupdate = $('input:text[id=username]').val().toString();
 
    var useremailupdate = $('input:text[id=useremail]').val().toString();
 
    var useridupdate = $("#id").val();
 
    db.transaction(function (tx) { tx.executeSql(updateStatement, [usernameupdate, useremailupdate, Number(useridupdate)], loadAndReset, onError); });
 
}
 
function dropTable() // Function Call when Drop Button Click.. Talbe will be dropped from database.
 
{
 
    db.transaction(function (tx) { tx.executeSql(dropStatement, [], showRecords, onError); });
 
    resetForm();
 
    initDatabase();
 
}
 
function loadRecord(i) // Function for display records which are retrived from database.
 
{
 
    var item = dataset.item(i);
 
    $("#username").val((item['username']).toString());
 
    $("#useremail").val((item['useremail']).toString());
 
    $("#id").val((item['id']).toString());
 
}
 
function resetForm() // Function for reset form input values.
 
{
 
    $("#username").val("");
 
    $("#useremail").val("");
 
    $("#id").val("");
 
}
 
function loadAndReset() //Function for Load and Reset...
 
{
 
    resetForm();
 
    showRecords()
 
}
 
function onError(tx, error) // Function for Hendeling Error...
 
{
 
    alert(error.message);
 
}
 
function showRecords() // Function For Retrive data from Database Display records as list
 {
 
    $("#normal_results").html('')
	$("#list_results").html('')
	$("#edit_results").html('')
 
    db.transaction(function (tx) {
 
        tx.executeSql(selectAllStatement, [], function (tx, result) {
 
            dataset = result.rows;
 			var libbook_normal = '';
			var libbook_list = '';
			var libbook_edit='';
			
			if(dataset.length == 0)
			{
			$("#normal_results").append('No books Found');
			$("#list_results").append('No books Found');
			$("#edit_results").append('No books Found');
			}
			else
			{
			
            for (var i = 0, item = null; i < dataset.length; i++) {
 
                item = dataset.item(i);
 
 				var libbook_normal ='<div class="library-stand"><div class="book-shelf"><img src='+item['bookimage']+ ' alt="Books" border="0" width="126" height="189" ></div></div>';
				
				var libbook_list ='<aside class="topbg"><div class="left"><img src='+item['bookimage']+ ' alt="lib-list-book" width="32" height="38"></div><div class="content-tab"><h1>'+item['bookname']+ '</h1><!--<h3>Category:</h3>--></div><div class="go-arrow"><img src="images/lib-list-arrow.png" alt="lib-list-arrow"></div></div></aside><div class="clear"></div>';
				
				var libbook_edit = '<div class="library-stand"><div class="close-btn"><img src="images/library-close-icon.png" onclick="delete_book('+item['id']+');" alt="close"></div><div class="book-shelf"><a href="#"><img src='+item['bookimage']+ ' alt="Books" border="0" width="126" height="189"></a></div></div>';
				
				//var libbook_list ='<img src='+item['bookimage']+ ' alt="lib-list-book">';
				

                $("#normal_results").append(libbook_normal);
				$("#list_results").append(libbook_list);
				$("#edit_results").append(libbook_edit);

            }
			}
 
        });
 
    });
 
}


function delete_book(id)
{
deleteRecord(id);
}




function loadAndReset() //Function for Load and Reset...
 
{
 
    resetForm();
 
    showRecords()
 
}
 
function onError(tx, error) // Function for Hendeling Error...
 
{
 
    alert(error.message);
 
}

// ================================  sqlite ends =====================================
function navigate_url(bookid)
{


    initDatabase();
	//dropTable();
	alert(bookid);
	alert('here');
	
	    db.transaction(function (tx) {
 
        tx.executeSql(selectidStatement, [bookid], function (tx, result) {
	    //tx.executeSql(deleteStatement, [id], showRecords, onError); alert("Delete Sucessfully"); });
 
            dataset = result.rows;
 
			if(dataset.length == 0)
			{			
			insertRecord(bookid);
			}
			else
			{
            for (var i = 0, item = null; i < dataset.length; i++) {
 
                item = dataset.item(i);
  				//alert(item['status']);
 				if(item['status'] == 'downloading')
				{
				alert('downloading');
				//alert(item['id']);
				}
				else
				{
				
				}				
 
            }
			}
 
        });
 
    });
	
 	//insertRecord(bookid);
    //$("#submitButton").click(insertRecord);  // Register Event Listener when button click.
 
    //$("#btnUpdate").click(updateRecord);
 
    //$("#btnReset").click(resetForm);
 
    //$("#btnDrop").click(dropTable);
 



}


function lib_search()
{
var str = $('#book_search').val();


db.transaction(function (tx) {
 
        tx.executeSql(searchStatement, [str], function (tx, result) {
 //alert(searchStatement);
            dataset = result.rows;

			var libbook_normal = '';
			if(dataset.length ==0)
			{
			$('#search_results').html('No Book Found');
			}
			else{
            for (var i = 0, item = null; i < dataset.length; i++) {
 
                item = dataset.item(i);
				
				var libbook_normal ='<div class="library-stand"><div class="book-shelf"><img src='+item['bookimage']+ ' alt="Books" border="0" width="126" height="189" ></div></div>';
				 
 				$('#search_results').html(libbook_normal);				
				$('#normal_results').hide();
				$('#search_results').show();
								
            }
			}
        });
 
    });


}

function list_search()
{
var str = $('#book_search').val();


db.transaction(function (tx) {
 
        tx.executeSql(searchStatement, [str], function (tx, result) {
 //alert(searchStatement);
            dataset = result.rows;

			var libbook_list = '';
			if(dataset.length ==0)
			{
			$('#search_results').html('No Book Found');
			}
			else{
            for (var i = 0, item = null; i < dataset.length; i++) {
 
                item = dataset.item(i);
								
				var libbook_list ='<aside class="topbg"><div class="left"><img src='+item['bookimage']+ ' alt="lib-list-book" width="32" height="38"></div><div class="content-tab"><h1>'+item['bookname']+ '</h1><!--<h3>Category:</h3>--></div><div class="go-arrow"><img src="images/lib-list-arrow.png" alt="lib-list-arrow"></div></div></aside><div class="clear"></div>';
								
 
 				$('#search_results').html(libbook_list);
				$('#list_results').hide();
				$('#search_results').show();
								
            }
			}
 
        });
 
    });


}

function edit_search()
{
var str = $('#book_search').val();


db.transaction(function (tx) {
 
        tx.executeSql(searchStatement, [str], function (tx, result) {
 //alert(searchStatement);
            dataset = result.rows;

			var libbook_edit='';
			if(dataset.length ==0)
			{
			$('#search_results').html('No Book Found');
			}
			else{
            for (var i = 0, item = null; i < dataset.length; i++) {
 
                item = dataset.item(i);
									
				var libbook_edit = '<div class="library-stand"><div class="close-btn"><img src="images/library-close-icon.png" onclick="delete_book('+item['id']+');" alt="close"></div><div class="book-shelf"><a href="#"><img src='+item['bookimage']+ ' alt="Books" border="0" width="126" height="189"></a></div></div>';
 
 				$('#search_results').html(libbook_edit);
				$("#edit_results").hide();
				$('#search_results').show();
								
            }
			}
 
        });
 
    });


}