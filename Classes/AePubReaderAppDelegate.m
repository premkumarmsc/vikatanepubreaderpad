//
//  AePubReaderAppDelegate.m
//  AePubReader
//
//  Created by PREMKUMAR on 04/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AePubReaderAppDelegate.h"
#import <GooglePlus/GooglePlus.h>
#import "File_Copy.h"
#import "WebViewController.h"
#import "ViewController.h"
#import "GAI.h"
#import "GAITrackedViewController.h"
@interface AePubReaderAppDelegate () <GPPDeepLinkDelegate>

@end
@implementation AePubReaderAppDelegate
@synthesize facebook;
static NSString * const kClientID =
@"373257897663.apps.googleusercontent.com";

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [GPPURLHandler handleURL:url
                  sourceApplication:sourceApplication
                         annotation:annotation];
}

#pragma mark - GPPDeepLinkDelegate

- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink {
    // An example to handle the deep link data.
    UIAlertView *alert = [[[UIAlertView alloc]
                           initWithTitle:@"Deep-link Data"
                           message:[deepLink deepLinkID]
                           delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil] autorelease];
    [alert show];
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
   
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 50;
    // Optional: set debug to YES for extra debugging information.
    [GAI sharedInstance].debug = YES;
    // Create tracker instance.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-44745265-1"];

       
    [GPPSignIn sharedInstance].clientID = kClientID;
       NSString *kAppID =@"371550522972620";
    
    Reachability1 *reach=[Reachability1 reachabilityWithHostName:@"www.vikatan.com"];
	
    NetworkStatus1 internetStatus=[reach currentReachabilityStatus];
	
    if ((internetStatus != ReachableViaWiFi1) && (internetStatus != ReachableViaWWAN1))
		
	{
		
        NSLog(@"Network is unreachable 12.");
        
        NSLog(@"Network is unreachable.");
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            if(result.height == 480)
            {
                // iPhone Classic
                
                NSLog(@"OLD IPHONE");
                self.detailViewController1 = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
                NSLog(@"iPhone 5");
            }
            if(result.height == 568)
            {
                // iPhone 5
                self.detailViewController1 = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
                NSLog(@"iPhone 5");
            }
        }
        else
        {
            NSLog(@"iPAd");
            
            self.detailViewController1 = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        }
        
        self.window.rootViewController = self.detailViewController1;
        [self.window makeKeyAndVisible];
	}
	
	else
		
	{

    
    if (![File_Copy alreadyFileExists])
    {
        
        File_Copy *fileCopy=[[File_Copy alloc]init];
        
        
        if ( [fileCopy copyFileToDocumentsFolder:@"Demo.zip"])
        {
            //NSLog(@"COPY SUCCESS");
        }
    }
    
    //WebViewController *web=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];

    
    //self.window = [[UIWindow alloc] initWithFrame:CGRectMake(0, 0, 320, 548)];
    // Override point for customization after application launch.
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            // iPhone Classic
            
            NSLog(@"OLD IPHONE");
            
            self.detailViewController = [[WebViewController alloc] initWithNibName:@"WebViewController~iPhone" bundle:nil];
            NSLog(@"iPhone 5");
            self.detailViewController.comingFrom=@"FIRST";
        }
        if(result.height == 568)
        {
            // iPhone 5
             self.detailViewController = [[WebViewController alloc] initWithNibName:@"WebViewController~iPhone" bundle:nil];
            NSLog(@"iPhone 5");
            self.detailViewController.comingFrom=@"FIRST";
        }
    }
    else
    {
        NSLog(@"iPAd");
        WebViewController *obj1 = [[WebViewController alloc]initWithNibName:@"WebViewController"bundle:Nil];
        facebook =[[Facebook alloc]initWithAppId:kAppID andDelegate:obj1];

         self.detailViewController = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
          self.detailViewController.comingFrom=@"FIRST";
    }

    
   
    self.window.rootViewController = self.detailViewController;
    [self.window makeKeyAndVisible];
    }
    
    // Read Google+ deep-link data.
    [GPPDeepLink setDelegate:self];
    [GPPDeepLink readDeepLinkAfterInstall];
    
    // Add registration for remote notifications
	[[UIApplication sharedApplication]
     registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
	
    
    NSLog(@"Application Finish");
    
	// Clear application badge when app launches
	application.applicationIconBadgeNumber = 0;
    
    return YES;
}

#pragma mark -
#pragma mark Application lifecycle


/*
 * --------------------------------------------------------------------------------------------------------------
 *  BEGIN APNS CODE
 * --------------------------------------------------------------------------------------------------------------
 */

/**
 * Fetch and Format Device Token and Register Important Information to Remote Server
 */



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    
    NSLog(@"ENTER");
    // 6bc2db2c1bf274bcceb4a93fcb84fb9cd5b1946f587b16209c66e8842e4bbb12 // My
    // bed8143d8257c904504476f0d09244492ee877c026d5738a82b80df68207c13a // iPhone
    // 7f6bce35595dd819e4019d3a4a9c96a25db706dd3686905173eb87ff413cd1f3 //iPOd
    // openssl pkcs12 -in dev.p12 -out dev.pem -nodes
    
    // openssl pkcs12 -in adhoc.p12 -out adhoc.pem -nodes
	/*
#if !TARGET_IPHONE_SIMULATOR
    
	// Get Bundle Info for Remote Registration (handy if you have more than one app)
	NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
	NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
	
	// Check what Notifications the user has turned on.  We registered for all three, but they may have manually disabled some or all of them.
	NSUInteger rntypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
	
	// Set the defaults to disabled unless we find otherwise...
	NSString *pushBadge = (rntypes & UIRemoteNotificationTypeBadge) ? @"enabled" : @"disabled";
	NSString *pushAlert = (rntypes & UIRemoteNotificationTypeAlert) ? @"enabled" : @"disabled";
	NSString *pushSound = (rntypes & UIRemoteNotificationTypeSound) ? @"enabled" : @"disabled";
	
	// Get the users Device Model, Display Name, Unique ID, Token & Version Number
	UIDevice *dev = [UIDevice currentDevice];
	//NSString *deviceUuid;
		NSString *deviceName = dev.name;
	NSString *deviceModel = dev.model;
	NSString *deviceSystemVersion = dev.systemVersion;
	
	// Prepare the Device Token for Registration (remove spaces and < >)
	NSString *deviceToken = [[[[devToken description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
	
    
    
    NSLog(@"DEVICE TOKEN:%@",deviceToken);
    
    NSUserDefaults *userValue=[NSUserDefaults standardUserDefaults];
    
    [userValue setObject:deviceToken forKey:@"DEVICE_TOKEN"];
    
     
     // com.vikatan.ebookreaderiphone.IAP.
    
    /*
     // Build URL String for Registration
     // !!! CHANGE "www.mywebsite.com" TO YOUR WEBSITE. Leave out the http://
     // !!! SAMPLE: "secure.awesomeapp.com"
     NSString *host = @"www.mywebsite.com";
     
     // !!! CHANGE "/apns.php?" TO THE PATH TO WHERE apns.php IS INSTALLED
     // !!! ( MUST START WITH / AND END WITH ? ).
     // !!! SAMPLE: "/path/to/apns.php?"
     NSString *urlString = [NSString stringWithFormat:@"/apns.php?task=%@&appname=%@&appversion=%@&deviceuid=%@&devicetoken=%@&devicename=%@&devicemodel=%@&deviceversion=%@&pushbadge=%@&pushalert=%@&pushsound=%@", @"register", appName,appVersion, deviceUuid, deviceToken, deviceName, deviceModel, deviceSystemVersion, pushBadge, pushAlert, pushSound];
     
     // Register the Device Data
     // !!! CHANGE "http" TO "https" IF YOU ARE USING HTTPS PROTOCOL
     NSURL *url = [[NSURL alloc] initWithScheme:@"http" host:host path:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
     NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
     NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
     //NSLog(@"Register URL: %@", url);
     //NSLog(@"Return Data: %@", returnData);
     */

}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	
#if !TARGET_IPHONE_SIMULATOR
	
	NSLog(@"Error in registration. Error: %@", error);
	
#endif
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	
#if !TARGET_IPHONE_SIMULATOR
    
    
    
    
	NSLog(@"remote notification: %@",[userInfo description]);
	NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
	
	NSString *alert = [apsInfo objectForKey:@"alert"];
	NSLog(@"Received Push Alert: %@", alert);
	
	NSString *sound = [apsInfo objectForKey:@"sound"];
	NSLog(@"Received Push Sound: %@", sound);
	//AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
	
	NSString *badge = [apsInfo objectForKey:@"badge"];
	NSLog(@"Received Push Badge: %@", badge);
	application.applicationIconBadgeNumber = [[apsInfo objectForKey:@"badge"] integerValue];
	
#endif
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [_window release];
    [super dealloc];
}


@end

